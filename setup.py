import os



if __name__ == "__main__":
# 1) Add env var for the haze directory
  haze_dir = os.path.dirname(os.path.realpath(__file__))
  env_vars_path = os.path.expanduser("~/.bashrc")
  with open (env_vars_path, mode) as file:
    file.write("\nhaze_dir = " + haze_dir + "\n")


# 2) Add file to nautilus scrypts
