from console_progressbar import ProgressBar
from scp import SCPClient
import paramiko
import getpass
import sys
import re

if len(sys.argv) != 2:
    print("Wrong number of arguments")
    sys.exit()

action = sys.argv[1]

if(action != "put" and action != "get"
    and action != "push" and action != "pull"):
    print("Unknown argument " + action)
    sys.exit()

port = 22
hostname = 'kiops.ru'
username = input("Username: ")
password = getpass.getpass("Password: ")

ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname=hostname, port=port, username=username, password=password)

def progress(filename, size, sent):
    progress = float(sent)/float(size)*100
    filename = filename.decode("utf-8") 
    pb = ProgressBar(total=100, prefix=filename, decimals=0, length=0)
    pb.print_progress_bar(progress)

scp = SCPClient(ssh.get_transport(), progress = progress)

if action == "put" or action == "push":
    
    scp.put('query_img', recursive= True, remote_path='haze_project_images')
    scp.put('train_img', recursive= True, remote_path='haze_project_images')
elif action == "get" or action == "pull":
        scp.get('haze_project_images/query_img', recursive= True)
        scp.get('haze_project_images/train_img', recursive= True)

scp.close()