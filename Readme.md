Steps to build

<!-- assuming you have opencv_contrib-master in ../opencv_contrib-master/ -->
<!-- Build as I did it-->
cmake -DCMAKE_BUILD_TYPE=Release \
-DCMAKE_INSTALL_PREFIX=/usr/local \
-DOPENCV_ENABLE_NONFREE:BOOL=ON \
-DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-master/modules ..
<!-- Or with CUDA -->
cmake -DCMAKE_BUILD_TYPE=Release \
-DCMAKE_INSTALL_PREFIX=/usr/local \
-DOPENCV_ENABLE_NONFREE:BOOL=ON \
-DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-master/modules \
-DWITH_CUDA=ON \
-DENABLE_FAST_MATH=1 \
-DCUDA_FAST_MATH=1 \
-DCMAKE_C_COMPILER="/usr/bin/clang" \
-DCMAKE_CXX_COMPILER="/usr/bin/clang++" \
-DWITH_CUBLAS=1 ..

make -j5
sudo make install

<!-- and install it in python --> 
python3 /python_lodaer/setup.py build
python3 /python_lodaer/setup.py install

<!-- with gstreamer: -->
cmake -DCMAKE_BUILD_TYPE=Release \
-DCMAKE_INSTALL_PREFIX=/usr/local \
-DOPENCV_ENABLE_NONFREE:BOOL=ON \
-DBUILD_JAVA=OFF \
-DWITH_GSTREAMER=ON \
-DWITH_FFMPEG=1 \
-DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-master/modules ..