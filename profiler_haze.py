import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
import cProfile

def surfTest():
    image_name = "eiffel"
    image_format = "jpg"
    img1 = cv.imread('query_img/{}.{}'.format(image_name, image_format), 0)  # queryImagejpg
    img2 = cv.imread('train_img/{}.{}'.format(image_name, image_format), 0)  # trainImage
    # Do i really need it?
    img1 = cv.resize(img1, (img2.shape[1], img2.shape[0])) 
    # Initiate SIFT detector
    surf = cv.xfeatures2d.SURF_create(400)
    # find the keypoints and descriptors with SIFT
    kp1, des1 = surf.detectAndCompute(img1,None)
    kp2, des2 = surf.detectAndCompute(img2,None)

def siftTest():
    image_name = "eiffel"
    image_format = "jpg"
    img1 = cv.imread('query_img/{}.{}'.format(image_name, image_format), 0)  # queryImagejpg
    img2 = cv.imread('train_img/{}.{}'.format(image_name, image_format), 0)  # trainImage
    # Do i really need it?
    img1 = cv.resize(img1, (img2.shape[1], img2.shape[0])) 
    # Initiate SIFT detector
    sift = cv.xfeatures2d.SIFT_create()
    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

def siftTest():
    image_name = "eiffel"
    image_format = "jpg"
    img1 = cv.imread('query_img/{}.{}'.format(image_name, image_format), 0)  # queryImagejpg
    img2 = cv.imread('train_img/{}.{}'.format(image_name, image_format), 0)  # trainImage
    print("Kiops, it's here!", img1.shape)

    img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)
    img2 = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)
    cv.imshow(img1)
    # Do i really need it?
    img1 = cv.resize(img1, (img2.shape[1], img2.shape[0])) 
    # Initiate SIFT detector
    sift = cv.xfeatures2d.SIFT_create()
    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

# cProfile.run('surfTest()')
cProfile.run('siftTest()')
cProfile.run('siftGreyTest()')