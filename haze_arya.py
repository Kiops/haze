import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

cv2 = cv

MIN_MATCH_COUNT = 5

image_name = "eiffel"
image_format = "png"

img1 = cv.imread('query_img/{}.{}'.format(image_name, image_format), 0)  # queryImagejpg
img2 = cv.imread('train_img/{}.{}'.format(image_name, image_format), 0)  # trainImage

# Do i really need it?
img1 = cv.resize(img1, (img2.shape[1], img2.shape[0])) 
img1_saved, img2_saved = img1, img2

# img1 = cv.resize(img1, (int(img2.shape[1]/2), int(img2.shape[0]/2)))
# img2 = cv.resize(img2, (int(img2.shape[1]/2), int(img2.shape[0]/2)))

# smoothing = 0
# kernel = np.ones((smoothing,smoothing),np.float32)/(smoothing**2)
# img1 = cv.filter2D(img1,-1,kernel)
# img2 = cv.filter2D(img2,-1,kernel)

# img1 = edges = cv.Canny(img1,50,150,apertureSize = 3)
# img2 = edges = cv.Canny(img2,50,150,apertureSize = 3)
# img1 = cv.Sobel(img1, cv2.CV_64F, 1, 0)
# img2 = cv.Sobel(img1, cv2.CV_64F, 0, 1)
# print(img1.shape)



# plt.imshow(img1, 'gray'),plt.show()
# plt.imshow(img2, 'gray'),plt.show()
# Initiate SIFT detector
sift = cv.xfeatures2d.SIFT_create()
surf = cv.xfeatures2d.SURF_create(400)
# img1 = cv.resize(img1, (int(img2.shape[1]/2), int(img2.shape[0]/2)))
# img2 = cv.resize(img2, (int(img2.shape[1]/2), int(img2.shape[0]/2)))

# orb = cv.ORB_create()


# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks = 50)

flann = cv.FlannBasedMatcher(index_params, search_params)

matches = flann.knnMatch(des1,des2,k=2)
# print("matches[0]", matches[0])
# for m,n in matches: print("m.trainIdx ", m.trainIdx , "m.queryIdx ", m.queryIdx, "m.distance", m.distance); break
# for m,n in matches: print("n.trainIdx ", n.trainIdx , "n.queryIdx ", n.queryIdx, "n.distance", n.distance ); break
good = []
for m,n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

print(len(good))

if len(good)>MIN_MATCH_COUNT:
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)


    M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()

    h,w = img1.shape
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
    dst = cv.perspectiveTransform(pts,M)

    print(M)
    warped = cv.warpPerspective(img1, M, (img1.shape[1], img1.shape[0]))
    img3 = cv.addWeighted(img2,0.5,warped,0.5,0)

    # Need to draw only good matches, so create a mask
    matchesMask = [[0,0] for i in range(len(matches))]
    # ratio test as per Lowe's paper
    for i,(m,n) in enumerate(matches):
        if m.distance < 0.7*n.distance:
            matchesMask[i]=[1,0]
    draw_params = dict(matchColor = (0,255,0),
                       singlePointColor = (255,0,0),
                       matchesMask  = matchesMask,
                   flags = 0)

    img3 = cv.drawMatchesKnn(img1,kp1,img2,kp2,matches,None,**draw_params)
    plt.imshow(img3, 'gray'),plt.show()

    # img2 = cv.polylines(img2,[np.int32(dst)],True,255,3, cv.LINE_AA)

else:
    print("Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT))
    matchesMask = None
draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)