import sys
if(len(sys.argv) < 3):
    sys.exit()

import time
import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
cv2 = cv

def get_features_to_track(img1, img2, num_of_dots= 10):
    cheat = False
    if(cheat):
        cheat_1 = [[[986.25 ,149.25]], [[635. ,178.]], [[952.5 ,423.]], [[633.75 ,313.]], [[850. ,320.5]]]
        cheat_2 = [[[1411.11 ,328.89]], [[940. ,366.67]], [[1366.67 ,708.89]], [[940. ,560.]], [[1226.67 ,571.11]]]
        img1_points = np.float32(cheat_1).reshape(-1,1,2)
        img2_points = np.float32(cheat_2).reshape(-1,1,2)
        return (img1_points, img2_points)
    surf = cv.xfeatures2d.SURF_create() #using surf instead of sift
    sift = cv.xfeatures2d.SIFT_create()
    print("Detecting features in img1..")
    kp1 = sift.detect(img1,None)
    kp1, des1 = surf.compute(img1, kp1)
    print("Done!")
    print("Detecting features in img2..")
    kp2 = sift.detect(img2,None)
    kp2, des2 = surf.compute(img2, kp2)
    print("Done!")


    print("Finding coresponding features..")
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    good = []
    print("Done!")
    print("Filtring and sorting coresponding features..")
    for m,n in matches:
        if m.distance < 0.70*n.distance:
            good.append(m)
    print("Num of good:", len(good))

    good.sort(key = lambda x:x.distance)
    img2_points = []
    print("Done!")
    if(len(good) < num_of_dots):
        num_of_dots = len(good)
    img1_points = np.float32([ kp1[m.queryIdx].pt for m in good[:num_of_dots]]).reshape(-1,1,2)
    img2_points = np.float32([ kp2[m.trainIdx].pt for m in good[:num_of_dots]]).reshape(-1,1,2)
    return (img1_points, img2_points)

def apply_image(src_pts, dst_pts, img1, img2):
    # print("apply", src_pts.shape, dst_pts.shape, img1.shape, img2.shape)
    M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()
    # print(matchesMask[0])

    h,w = img1.shape
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
    dst = cv.perspectiveTransform(pts,M)

    warped = cv.warpPerspective(img1, M, (img2.shape[1], img2.shape[0]))
    warped = cv.cvtColor(warped, cv2.COLOR_GRAY2RGB)
    # print(img2.shape, warped.shape)
    img3 = cv.addWeighted(img2,0.5,warped,0.5,0)
    return img3

image_name = sys.argv[1]
video_name = sys.argv[2]

#Load image and the first frame
#Get features of image and the first frame
#Compare them and find homography
#Set trackers on main features
#For each frame: get location of trackers, calculate diff from their previous position and correct position of image accroding to it



image = cv.cvtColor(cv2.imread(image_name), cv.COLOR_BGR2GRAY)
cap = cv.VideoCapture(video_name)

video_width = cap.get(cv.CAP_PROP_FRAME_WIDTH)
video_height = cap.get(cv.CAP_PROP_FRAME_HEIGHT)
video_diagonal = (video_width**2 + video_height**2)**(1/2)
print(video_width, video_height)
image_diagonal = (image.shape[0]**2 + image.shape[1]**2)**(1/2)
print(image.shape[0], image.shape[1])
proportion = image_diagonal/video_diagonal

fourcc = cv2.VideoWriter_fourcc(*'MP4V')
out_video = cv2.VideoWriter('output.mp4', 0x00000021, cap.get(cv.CAP_PROP_FPS), (int(video_width),int(video_height)))
# params for ShiTomasi corner detection
feature_params = dict( maxCorners = 100,
                       qualityLevel = 0.3,
                       minDistance = 7,
                       blockSize = 7 )
# Parameters for lucas kanade optical flow
lk_params = dict( winSize  = (30,30),
                  maxLevel = 2,
                  criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))
# Create some random colors
color = np.random.randint(0,255,(100,3))
# Take first frame and find corners in it
ret, old_frame = cap.read()
old_gray = cv.cvtColor(old_frame, cv.COLOR_BGR2GRAY)
# image = cv.resize(image, (int(image.shape[1]/proportion), int(image.shape[0]/proportion)))
print(image.shape)
# p0 = cv.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
image_points, p0 = get_features_to_track(image, old_gray, 50)

# Create a mask image for drawing purposes
mask = np.zeros_like(old_frame)
counter = 0
start = time.time()
times = []
while(cap.isOpened()):
    local_start = time.time()
    counter += 1
    print(counter)
    ret,frame = cap.read()
    if(not ret):
        break
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    # calculate optical flow
    p1, st, err = cv.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)
    # Select good points
    good_new = p1[st==1]
    good_old = p0[st==1]

    # remove lost features
    indexes = []
    for index, value in enumerate(st):
        if(value != 1):
            indexes.append(index)
    if(indexes):
        image_points = np.delete(image_points, indexes, axis=0)

    # print("good_new, good_old: ", len(good_new), len(good_old))
    # draw the tracks
    # for i,(new,old) in enumerate(zip(good_new,good_old)):
        # a,b = new.ravel()
        # c,d = old.ravel()
        # mask = cv.line(mask, (a,b),(c,d), color[i].tolist(), 2)
        # frame = cv.circle(frame,(a,b),5,color[i].tolist(),-1)
    img = cv.add(frame,mask)
    frame = apply_image(image_points, good_new, image, frame)
    out_video.write(frame)
    # Now update the previous frame and previous points
    old_gray = frame_gray.copy()
    p0 = good_new.reshape(-1,1,2)
    times.append(time.time()-local_start)
    # if(counter == 630):
    #     break
# cv.destroyAllWindows()
print("Elapsed time:", time.time()-start)
print("Avarage frame delay:", sum(times)/len(times))
cap.release()
out_video.release()

# img1 = cv.imread('barristan_data/{}.{}'.format(image_name), 0)  # queryImagejpg
# img2 = cv.imread('barristan_data/{}.{}'.format("screenshot", image_format), 0)  # trainImage



#Get 