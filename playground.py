import sys
if(len(sys.argv) < 3):
    sys.exit()

import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
cv2 = cv

def make_mask(img1, img2):
    # img1 = cv.cvtColor(img1, cv2.COLOR_GRAY2RGB)
    img2 = cv.cvtColor(img2, cv2.COLOR_GRAY2RGB)
    # img2 = cv2.blur(img2,(5,5))
    # img1 = cv2.blur(img1,(5,5))
    print(img1.shape, img2.shape)
    backSub = cv.createBackgroundSubtractorMOG2()
    # backSub = cv.createBackgroundSubtractorKNN()

    diff_mask = backSub.apply(img1)
    diff_mask = backSub.apply(img2)

    diff_mask_rgb = cv2.cvtColor(diff_mask, cv.COLOR_GRAY2RGB)
    share_mask = cv.subtract(img1, diff_mask_rgb)
    share_mask = cv.subtract(img1, diff_mask_rgb)
    print(share_mask.shape)

    # share_32 = np.float32(share_mask)
    # criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    # K = 8
    # ret,label,center=cv2.kmeans(share_32,K,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS)
    # print(ret, label.shape, center.shape)


    plt.imshow(share_mask), plt.show()

def crop_mask(image, x1,y1,x2,y2):
    image = cv.cvtColor(image, cv2.COLOR_GRAY2RGB)
    mask = np.zeros_like(image)
    print(mask.shape)
    mask[y1:y2, x1:x2] = image[y1:y2, x1:x2]
    return mask

def get_features_to_track(img1, img2, num_of_dots= 10):
    sift = cv.xfeatures2d.SIFT_create()
    print("Detecting features in img1..")
    kp1, des1 = sift.detectAndCompute(img1,None)
    print("Done!")
    print("Detecting features in img2..")
    kp2, des2 = sift.detectAndCompute(img2,None)
    print("Done!")


    print("Finding coresponding features..")
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    good = []
    print("Done!")
    print("Filtring and sorting coresponding features..")
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)

    def getDistance(elem):
        return elem.distance
    good.sort(key = getDistance)
    img2_points = []
    print("Done!")
    if(len(good) < num_of_dots):
        num_of_dots = len(good)
    img1_points = np.float32([ kp1[m.queryIdx].pt for m in good[:num_of_dots]]).reshape(-1,1,2)
    img2_points = np.float32([ kp2[m.trainIdx].pt for m in good[:num_of_dots]]).reshape(-1,1,2)
    # img2_points = [[[1438., 366.]],[[1366., 428.]],[[ 990., 377.]],[[1342., 341.]],[[ 696., 400.]],[[1072., 314.]],[[ 415., 401.]],[[ 454., 342.]],[[ 432., 314.]],[[1276., 338.]]]
    # img2_points = np.around(img2_points)
    return (img1_points, img2_points)

def get_warped(src_pts, dst_pts, img1, img2):
    M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()

    h,w = img1.shape
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
    dst = cv.perspectiveTransform(pts,M)

    warped = cv.warpPerspective(img1, M, (img2.shape[1], img2.shape[0]))
    warped = cv.cvtColor(warped, cv2.COLOR_GRAY2RGB)

    return warped

def apply_image(src_pts, dst_pts, img1, img2):
    warped = get_warped(src_pts, dst_pts, img1, img2)
    # print(img2.shape, warped.shape)
    img3 = cv.addWeighted(img2,0.5,warped,0.5,0)
    return img3

image_name = sys.argv[1]
video_name = sys.argv[2]

#Load image and the first frame
#Get features of image and the first frame
#Compare them and find homography
#Set trackers on main features
#For each frame: get location of trackers, calculate diff from their previous position and correct position of image accroding to it

image = cv.cvtColor(cv2.imread(image_name), cv.COLOR_BGR2GRAY)
# mask_cropped = crop_mask(image, 863, 1083, 1606,2788)
# print(image.shape, mask_cropped.shape)
# plt.imshow(mask, 'gray'), plt.show()
cap = cv.VideoCapture(video_name)
fourcc = cv2.VideoWriter_fourcc(*'MP4V')
out_video = cv2.VideoWriter('output.mp4', 0x00000021, cap.get(cv.CAP_PROP_FPS), (int(cap.get(cv.CAP_PROP_FRAME_WIDTH)),int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))))
# params for ShiTomasi corner detection
feature_params = dict( maxCorners = 100,
                       qualityLevel = 0.3,
                       minDistance = 7,
                       blockSize = 7 )
# Parameters for lucas kanade optical flow
lk_params = dict( winSize  = (30,30),
                  maxLevel = 2,
                  criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))
# Create some random colors
color = np.random.randint(0,255,(100,3))
# Take first frame and find corners in it
ret, old_frame = cap.read()
old_gray = cv.cvtColor(old_frame, cv.COLOR_BGR2GRAY)
image = cv.resize(image, (old_gray.shape[1], old_gray.shape[0]))
# mask_cropped = cv.resize(mask_cropped, (old_gray.shape[1], old_gray.shape[0]))
# mask_cropped = cv.cvtColor(mask_cropped, cv.COLOR_RGB2GRAY)
# p0 = cv.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
image_points, p0 = get_features_to_track(image, old_gray, 50)

warped = get_warped(image_points, p0, image, old_frame)
shared_mask = make_mask(warped, old_gray)
sys.exit()

# Create a mask image for drawing purposes
mask = np.zeros_like(old_frame)
counter = 0
while(cap.isOpened()):
    counter += 1
    print(counter)
    ret,frame = cap.read()
    if(not ret):
        break
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    # calculate optical flow
    p1, st, err = cv.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)
    # Select good points
    good_new = p1[st==1]
    good_old = p0[st==1]

    # remove_lost_features
    for index, value in enumerate(st):
        if(value != 1):
            image_points = np.delete(image_points, index, axis=0)

    # print("good_new, good_old: ", len(good_new), len(good_old))
    # draw the tracks
    for i,(new,old) in enumerate(zip(good_new,good_old)):
        a,b = new.ravel()
        c,d = old.ravel()
        mask = cv.line(mask, (a,b),(c,d), color[i].tolist(), 2)
        # frame = cv.circle(frame,(a,b),5,color[i].tolist(),-1)
    img = cv.add(frame,mask)
    frame = apply_image(image_points, good_new, image, frame)

    out_video.write(frame)
    # Now update the previous frame and previous points
    old_gray = frame_gray.copy()
    p0 = good_new.reshape(-1,1,2)
    # if(counter == 630):
    #     break
# cv.destroyAllWindows()
cap.release()
out_video.release()

# img1 = cv.imread('barristan_data/{}.{}'.format(image_name), 0)  # queryImagejpg
# img2 = cv.imread('barristan_data/{}.{}'.format("screenshot", image_format), 0)  # trainImage



#Get 